package no.uib.inf101.terminal;

public class CmdEcho implements Command {

    @Override
    public String run(String[] args) {

        String returnedString = "";

        for (String string : args) {
            returnedString += string;
            returnedString += " ";         
        }
        return returnedString;
    }

    @Override
    public String getName() {
        return "echo";
    }

    
    
}
